<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php bloginfo('name'); wp_title( '|', true, 'right' ); ?></title>
<meta http-equiv="content-language" content="fa" />
<?php wp_head(); ?>
</head>
<body>
<h1>list of the books</h1>
<table style="width: 100%">
<thead style="font-weight: bold;">
<tr><td>ID</td><td>Title</td><td>ISBN</td><td>Authors</td><td>Publisher</td></tr>
</thead>
<tbody>
<?php

if (have_posts() ) : while (have_posts() ) : the_post();
        $value = 0;
        $table_name = $wpdb->prefix . 'books_info';

        $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE post_id = %d", get_the_ID()));
        foreach($data as $val){
            $value = $val->isbn;
        }
		?>
        <tr><td><?php the_ID(); ?></td><td><?php the_title(); ?></td><td><?php echo $value;?></td>
        <td><?php $terms = get_the_terms( get_the_ID() , 'author' ); 
            foreach ( $terms as $term ) {
                $term_link = get_term_link( $term, 'author' );
            echo '<a href="' . $term_link . '">' . $term->name . '</a>';
            } 
        ?>
        </td>
        <td><?php $terms = get_the_terms( get_the_ID() , 'publisher' ); 
            foreach ( $terms as $term ) {
            $term_link = get_term_link( $term, 'publisher' );
            echo '<a href="' . $term_link . '">' . $term->name . '</a>';
            } 
        ?></td></tr>
         <?php endwhile; endif ; wp_reset_postdata();?>
</tbody>
</table>
</body>