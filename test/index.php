<?php
/*
Plugin Name: my Test
Plugin URI: http://127.0.0.1
Description: My test plugin
Version: 1
Author: Hadi salati
Author URI: http://127.0.0.1
License: GPL
Download URL: http://127.0.0.1
*/

register_activation_hook( __FILE__, 'activate_mytest' );

add_option( "my_test_init", "0" );

function activate_mytest(){
    global $wpdb;

    if(get_option( "my_test_init" ) == "0"){

        $table_name = $wpdb->prefix . 'books_info';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            post_id int NOT NULL,
            isbn int NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";
    
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        update_option( "my_test_init", "1" );
    }
}


function books() {

	$labels = array(
		'name'                  => 'books',
		'singular_name'         => 'book',
		'menu_name'             => 'books',
		'name_admin_bar'        => 'book'
	);
	$args = array(
		'label'                 => 'book',
		'description'           => 'all books',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'books', $args );

}
add_action( 'init', 'books', 0 );


function mt_taxonomy1() {

	$labels = array(
		'name'                       => 'Publishers',
		'singular_name'              => 'Publisher',
		'menu_name'                  => 'Publisher'
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rest_base'                  => 'Publisher',
	);
    register_taxonomy( 'publisher', array( 'books' ), $args );
}
 
function mt_taxonomy2() {   
    $labels = array(
		'name'                       => 'Authors',
		'singular_name'              => 'Author',
		'menu_name'                  => 'Author'
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rest_base'                  => 'Author',
	);
    register_taxonomy( 'author', array( 'books' ), $args );
}
add_action( 'init', 'mt_taxonomy1', 0 );
add_action( 'init', 'mt_taxonomy2', 0 );

function isbn_meta_box() {

    add_meta_box(
        'isbn',
        'isbn',
        'isbn_meta_box_callback',
        'books'
    );

}

add_action( 'add_meta_boxes_books', 'isbn_meta_box' );

function isbn_meta_box_callback( $post ) {
    global $wpdb;
    wp_nonce_field( basename( __FILE__ ), 'event_fields' );
    $value = 0;
    $table_name = $wpdb->prefix . 'books_info';

    $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE post_id = %d", $post->ID));
    foreach($data as $val){
        $value = $val->isbn;
    }
    echo '<input style="width:100%" id="isbn" type="number" name="isbn" value="' . esc_attr( $value ) . '" />';
}

function mt_save_events_meta( $post_id, $post ) {
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
    }
    
	if ( ! isset( $_POST['isbn'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
    }

    if ( 'revision' === $post->post_type ) {
        return;
    }

    global $wpdb;
    $table_name = $wpdb->prefix . 'books_info';

    $data = $wpdb->get_results("SELECT * FROM $table_name WHERE post_id = '".$post_id."'");

    if ( $wpdb->num_rows > 0 ) {
        $wpdb->update(
            $table_name,
            array('isbn' => $_POST['isbn']),
            array('post_id'=>$post_id)
        );
    } else {
         $wpdb->insert( 
            $table_name, 
            array( 
                'post_id' => $post_id, 
                'isbn' => $_POST['isbn'], 
            ) 
        );
    }
    if ( ! $_POST['isbn'] && $wpdb->num_rows > 0 ) {
        $wpdb->delete( $table_name, array( 'post_id' => $post_id ) );
        
    }
}
add_action( 'save_post', 'mt_save_events_meta', 1, 2 );

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
class Books_List extends WP_List_Table {

	public function __construct() {
		parent::__construct( [
			'singular' =>  'book', //singular name of the listed records
			'plural'   => 'books', //plural name of the listed records
			'ajax'     => false //should this table support ajax?
		] );
    }
    public static function get_books( $per_page = 10, $page_number = 1 ) {

        global $wpdb;
      
        $sql = "SELECT * FROM {$wpdb->prefix}books_info";
      
        if ( ! empty( $_REQUEST['orderby'] ) ) {
          $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
          $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
        }
      
        $sql .= " LIMIT $per_page";
      
        $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
      
      
        $result = $wpdb->get_results( $sql, 'ARRAY_A' );
      
        return $result;
      }
    
      public static function delete_book( $id ) {
        global $wpdb;
      
        $wpdb->delete(
          "{$wpdb->prefix}books_info",
          [ 'id' => $id ],
          [ '%d' ]
        );
      }

      public static function record_count() {
        global $wpdb;
      
        $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}books_info";
      
        return $wpdb->get_var( $sql );
      }
      public function no_items() {
        'No books avaliable.';
      }
      function column_name( $item ) {

        // create a nonce
        $delete_nonce = wp_create_nonce( 'delete_book' );
      
        $title = '<strong>' . $item['name'] . '</strong>';
      
        $actions = [
          'delete' => sprintf( '<a href="?page=%s&action=%s&book=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['id'] ), $delete_nonce )
        ];
      
        return $title . $this->row_actions( $actions );
      }
      public function column_default( $item, $column_name ) {
        switch ( $column_name ) {
          case 'post_id':
            return get_the_title($item[ $column_name ]);
          case 'isbn':
            return $item[ $column_name ];
          default:
            return print_r( $item, true ); //Show the whole array for troubleshooting purposes
        }
      }
      function column_cb( $item ) {
        return sprintf(
          '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
        );
      }
      function get_columns() {
        $columns = [
          'cb'      => '<input type="checkbox" />',
          'isbn'    => 'ISBN',
          'post_id' => 'Post'
        ];
      
        return $columns;
      }
      public function get_bulk_actions() {
        $actions = [
          'bulk-delete' => 'Delete'
        ];
      
        return $actions;
      }
      public function prepare_items() {

        $this->_column_headers = $this->get_column_info();
      
        /** Process bulk action */
        $this->process_bulk_action();
      
        $per_page     = $this->get_items_per_page( 'books_per_page', 5 );
        $current_page = $this->get_pagenum();
        $total_items  = self::record_count();
      
        $this->set_pagination_args( [
          'total_items' => $total_items, //WE have to calculate the total number of items
          'per_page'    => $per_page //WE have to determine how many items to show on a page
        ] );
      
      
        $this->items = self::get_books( $per_page, $current_page );
      }
      public function process_bulk_action() {

        //Detect when a bulk action is being triggered...
        if ( 'delete' === $this->current_action() ) {
      
          // In our file that handles the request, verify the nonce.
          $nonce = esc_attr( $_REQUEST['_wpnonce'] );
      
          if ( ! wp_verify_nonce( $nonce, 'delete_book' ) ) {
            die( 'Go get a life script kiddies' );
          }
          else {
            self::delete_book( absint( $_GET['book'] ) );
      
            wp_redirect( esc_url( add_query_arg() ) );
            exit;
          }
      
        }
      
        // If the delete bulk action is triggered
        if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
             || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
        ) {
      
          $delete_ids = esc_sql( $_POST['bulk-delete'] );
      
          // loop over the array of record IDs and delete them
          foreach ( $delete_ids as $id ) {
            self::delete_book( $id );
      
          }
      
          wp_redirect( esc_url( add_query_arg() ) );
          exit;
        }
      }

}

class mt_Plugin {

	// class instance
	static $instance;

	public $books_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
    }
    
    public static function set_screen( $status, $option, $value ) {
        return $value;
    }
    
    public function plugin_menu() {
    
        $hook = add_menu_page(
            'Books list',
            'Books list',
            'manage_options',
            'wp_list_table_class',
            [ $this, 'plugin_settings_page' ]
        );
    
        add_action( "load-$hook", [ $this, 'screen_option' ] );
    
    }
    public function screen_option() {

        $option = 'per_page';
        $args   = [
            'label'   => 'Books',
            'default' => 10,
            'option'  => 'Books_per_page'
        ];
    
        add_screen_option( $option, $args );
    
        $this->books_obj = new Books_List();
    }
    public function plugin_settings_page() {
        ?>
        <div class="wrap">
            <h2>Books list</h2>
    
            <div id="poststuff">
                <div id="post-body" class="metabox-holder columns-2">
                    <div id="post-body-content">
                        <div class="meta-box-sortables ui-sortable">
                            <form method="post">
                                <?php
                                $this->books_obj->prepare_items();
                                $this->books_obj->display(); ?>
                            </form>
                        </div>
                    </div>
                </div>
                <br class="clear">
            </div>
        </div>
    <?php
    }
    public static function get_instance() {
        if ( ! isset( self::$instance ) ) {
            self::$instance = new self();
        }
    
        return self::$instance;
    }

}
add_action( 'plugins_loaded', function () {
	mt_Plugin::get_instance();
} );
