<?php
/*
Template Name: find
*/?>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php bloginfo('name'); wp_title( '|', true, 'right' ); ?></title>
<meta http-equiv="content-language" content="fa" />
<?php wp_head(); ?>
</head>
<body>
    <h1>Looking for:</h1>
    <form method=post>
        <input type="number" name="isbn" /><input type="submit" value="submit" name="submit" />
    </form>
    <table style="width: 100%">
<thead style="font-weight: bold;">
<tr><td>ID</td><td>Title</td><td>ISBN</td><td>Authors</td><td>Publisher</td></tr>
</thead>
<tbody>
    <?php if(!empty($_POST['submit'])){
        $table_name = $wpdb->prefix . 'books_info';

        $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE isbn = %d limit 0,1", $_POST['isbn']));
        
        if ( $wpdb->num_rows > 0 ) {
            
            $value = $data[0]->isbn;
		?>
        <tr><td><?php echo $data[0]->post_id; ?></td><td><?php echo get_the_title($data[0]->post_id); ?></td><td><?php echo $value;?></td>
        <td><?php $terms = get_the_terms( $data[0]->post_id , 'author' ); 
            foreach ( $terms as $term ) {
                $term_link = get_term_link( $term, 'author' );
            echo '<a href="' . $term_link . '">' . $term->name . '</a>';
            } 
        ?>
        </td>
        <td><?php $terms = get_the_terms( $data[0]->post_id , 'publisher' ); 
            foreach ( $terms as $term ) {
            $term_link = get_term_link( $term, 'publisher' );
            echo '<a href="' . $term_link . '">' . $term->name . '</a>';
            } 
        ?></td></tr>
         <?php 
        }else{
            echo "<h2>not found</h2>";
        }
    }?>
</tbody>
</table>
</body>
</html>